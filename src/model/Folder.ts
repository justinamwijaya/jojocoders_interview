export {}
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FolderSchema = new Schema({
    id:String,
    name:String,
    type:String,
    owner_id:String,
    company_id:String,
    content: Object
},{ timestamps:true })

const Folder = mongoose.model('Folder',FolderSchema)

module.exports = { Folder,FolderSchema }