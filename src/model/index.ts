export {}
const { Folder, FolderSchema } = require('./Folder.ts')
const { File, FileSchema } = require('./File.ts')

module.exports = { Folder, File }