export {}
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FileSchema = new Schema({
    id:String,
    name:String,
    type:String,
    owner_id:String,
    company_id:String,
    folder_id: String,
    content: Object,
    share: Array
},{ timestamps:true })

const File = mongoose.model('File',FileSchema)

module.exports = { File,FileSchema }