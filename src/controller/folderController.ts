export {}
const { File, Folder } = require('../model')

class FolderController {
    static setFolder (req: any, res: any) {
        try {
            let { id, name, timestamp } = req.body
            let owner_id = req.user.user_id
            let company_id = req.user.company_id

            Folder.findOne({ id })
            .then((result: any) => {
                if(result) {
                    result.name = name
                    result.timestamp = timestamp

                    result.save((e: any) => {
                        if(e) throw e;
                        res.status(200).json({
                            error: false,
                            message: `folder updated`,
                            result
                        })
                    })
                } else {
                    let folder = new Folder({
                        id,
                        name,
                        timestamp,
                        owner_id,
                        company_id
                    })

                    Folder.create(folder)
                    .then((result: any) => {
                        res.status(200).json({
                            error:false,
                            message: `folder created`,
                            result
                        })
                    })
                    .catch((e: any) => {
                        throw e
                    })
                }
            })
            .catch((e: any) => {
                throw e
            })
        } catch (error) {
            res.status(500).json({ error })
        }
    }

    static deleteFolder (req: any, res: any) {
        try {
            const { id } = req.body
            Folder.findOneAndDelete({ id })
            .then(() => {
                res.status(200).json({
                    error: false,
                    message: `Deleted folder Successfully`
                })
            })
            .catch((e: any) => {
                throw e
            })
        } catch (error: any) {
            res.status(500).json({ error })
        }
    }

    static listFilePerFolder (req: any, res: any) {
        try {
            const { folder_id } = req.params

            File.find({ folder_id })
            .then((result: any) => {
                res.status(200).json({
                    error:false,
                    result
                })
            })
            .catch((e: any) => {
                throw e
            })
        } catch (error: any) {
            res.status(500).json({ error })
        }
    }
}

module.exports = FolderController
