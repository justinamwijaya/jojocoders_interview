export {}
const GeneralController = require('./generalController.ts')
const FileController = require('./fileController.ts')
const FolderController = require('./folderController.ts')

module.exports = { GeneralController, FileController, FolderController }