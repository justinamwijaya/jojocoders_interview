export {}
const { File, Folder } = require('../model')

class GeneralController {
    static getAll (req: any, res: any) {
        try {
            File.find()
            .then((fileResult: object[]) => {
                Folder.find()
                .then((folderResult: object[]) => {
                    let data = fileResult.concat(folderResult)
                    res.status(200).json({
                        error: false,
                        data
                    })
                })
                .catch((e: any) => {
                    throw e
                })
            })
            .catch((e: any) => {
                throw e
            })

        } catch (error) {
            res.status(500).json({ error })
        }
    }
}

module.exports = GeneralController
