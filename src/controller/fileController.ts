export {}
const { File, Folder } = require('../model')

class FileController {
    static setFile (req: any, res: any) {
        try {
            let { id, name, type, timestamp, folder_id, content, share } = req.body
            let { company_id, owner_id } = req.user

            File.findOne({ id })
            .then((result: any) => {
                if (result) {
                    result.name = name
                    result.type = type
                    result.timestamp = timestamp
                    result.owner_id = owner_id
                    result.company_id = company_id
                    result.folder_id = folder_id
                    result.content = content
                    result.share = share

                    result.save((e: any) => {         
                        if(e) throw e;

                        res.status(200).json({
                            error: false,
                            messsage: `file edited`
                        })
                    })
                } else {
                    let file = new File({
                        id,
                        name,
                        type,
                        timestamp,
                        company_id,
                        folder_id,
                        content,
                        share
                    })

                    File.create(file)
                    .then((result: any) => {
                        res.status(200).json({
                            error: false,
                            message: `file created`
                        })
                    })
                    .catch((e: any) => {         throw e })
                }
            })
            .catch((e: any) => { throw e })
        } catch (error: any) {
            res.status(500).json({ error })
        }
    }

    static detailFile (req: any, res: any) {
        try {
            const { document_id } = req.params
            File.findOne({ id: document_id })
            .then((result: any) => {
                if(result) res.status(200).json({
                    error: false,
                    result
                })

                if(!result) res.status(404).json({
                    error: true,
                    message: `file not found`
                })
            })
            .catch((e: any) => { throw e })
        } catch(error: any) {
            res.status(500).json({ error })
        }
    }
    
    static deleteFile (req: any, res: any) {
        try {
            let { id } = req.body

            File.findOneAndDelete({ id })
            .then(() => {
                res.status(200).json({
                    error: false,
                    message: `file deleted`
                })
            })
            .catch((e: any) => { throw e })
        } catch (error: any) {
            res.status(500).json({ error })
        }
    }
}

module.exports = FileController
