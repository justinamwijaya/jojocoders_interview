const dotenv = require('dotenv')
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const app = express()
const routes = require('./routes');
const port = (process.env.PORT || 3000)

dotenv.config()

mongoose.connect(`mongodb://localhost:27017/${process.env.MONGODB_DB_NAME}`, {useNewUrlParser: true, useUnifiedTopology: true})

app.use(cors())

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({ extended: false }))
app.use('/', routes)

app.listen(port, console.log(`listening to ${port}`))
