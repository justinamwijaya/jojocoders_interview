const jwt = require('jsonwebtoken')

const authorize = (req: any, res: any, next: any) => {
    let token
    if(req.headers.authorization === undefined){
        res.status(403).json({
            msg: "you have not logged in!"
        })
    }
    else {
        token = req.headers.authorization.split(" ")[1]
    }
    jwt.verify(token,process.env.SECRET_KEY,(err: any, decoded: any)=>{
        if(err) {
            res.status(403).json({
                msg:"there's a problem with your authentication",
                err
            })
        }else{
            req.user = decoded
            next()
        }
    })
}

module.exports = {
    authorize
}