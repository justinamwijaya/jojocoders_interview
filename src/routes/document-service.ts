export {}
const router = require('express').Router();
const { GeneralController, FileController, FolderController } = require('../controller');

router.get('/', GeneralController.getAll);

router.post('/folder', FolderController.setFolder);

router.delete('/folder', FolderController.deleteFolder);

router.get('/folder/:folder_id', FolderController.listFilePerFolder);

router.post('/document', FileController.setFile);

router.get('/document/:document_id', FileController.detailFile);

router.delete('/document', FileController.deleteFile);

module.exports = router