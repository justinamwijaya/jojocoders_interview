export {}
const router = require('express').Router();
const documentServiceRoutes = require('./document-service.ts');
const { authorize } = require('../middlewares');

router.use('/document-service', authorize, documentServiceRoutes);

module.exports = router;